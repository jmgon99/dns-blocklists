# DNS blocklists
Domain blocklists for your DNS server.
Pihole ready.

<details><summary>Other blocklists</summary>
https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts

https://gist.githubusercontent.com/wassname/78eeaaad299dc4cddd04e372f20a9aa7/raw/36d217ab1d6e123d8fc2b39e48af8ca63e4d4ac8/LG%2520Smart-TV%2520Blocklist%2520Adlist%2520(for%2520PiHole)

https://raw.githubusercontent.com/jmw6773/wiiublocklist/master/wiiu_blacklist

https://gist.githubusercontent.com/hkamran80/779019103fcd306979411d44c8d38459/raw/3c168e68f067f00df825c73042255e6d3fd541c4/SmartTV2.txt

https://perflyst.github.io/PiHoleBlocklist/SmartTV.txt

https://perflyst.github.io/PiHoleBlocklist/android-tracking.txt

https://perflyst.github.io/PiHoleBlocklist/regex.list

https://perflyst.github.io/PiHoleBlocklist/SmartTV-AGH.txt

https://raw.githubusercontent.com/InAUGral/pihole-blocklist-gametrackers/main/pihole-blocklist-gametrackers.txt

https://raw.githubusercontent.com/phoanglong/ps4-dns-block/main/ps4-block
</details>
